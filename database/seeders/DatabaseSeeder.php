<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class DatabaseSeeder.
 * Тестовые данные треков времени.
 *
 * @package Database\Seeders
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $trackTypes      = DB::table('track_types')
            ->select(['code', 'id'])
            ->get();
        $trackTypesAssoc = [];

        foreach ($trackTypes as $trackType) {
            $trackTypesAssoc[$trackType->code] = $trackType->id;
        }

        $reportId = DB::table('reports')->insertGetId(
            [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'name'       => '16.08.2012',
            ]
        );

        $data = [
            [
                'name'       => 'Отдых',
                'type_id'    => $trackTypesAssoc['useless'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Не работает доставка Laguna',
                'type_id'    => $trackTypesAssoc['undefined'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Переговоры с клиентом Legacy',
                'type_id'    => $trackTypesAssoc['useful'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Сломался докер',
                'type_id'    => $trackTypesAssoc['useless'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Рефакторинг модуля поиска на проекте Учет времени',
                'type_id'    => $trackTypesAssoc['useless'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Форма запроса цены на проекте Legacy',
                'type_id'    => $trackTypesAssoc['useful'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Оценка по проектирования новой архитектуры ApiBot',
                'type_id'    => $trackTypesAssoc['useful'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Консультация по проекту Legacy',
                'type_id'    => $trackTypesAssoc['useful'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Афанасий чат',
                'type_id'    => $trackTypesAssoc['useless'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Тушение пожара на проекте Laguna',
                'type_id'    => $trackTypesAssoc['undefined'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Митап Docker',
                'type_id'    => $trackTypesAssoc['useless'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Принятие домашки у Христофора',
                'type_id'    => $trackTypesAssoc['undefined'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Афанасий чат',
                'type_id'    => $trackTypesAssoc['undefined'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
            [
                'name'       => 'Не работает доставка Laguna',
                'type_id'    => $trackTypesAssoc['undefined'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()->addHour(),
                'date_start' => Carbon::now(),
                'date_end'   => Carbon::now()->addHour(),
                'report_id'  => $reportId,
            ],
        ];

        foreach ($data as $datum) {
            DB::table('tracks')->insert($datum);
        }
    }
}
