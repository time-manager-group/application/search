<?php

namespace App\Repositories\DTO;

use App\Services\TracksFinder\Interfaces\ITrackRepositoryResultItem;

/**
 * Class TrackRepositoryResultItem.
 * Dto результатов репозитория треков времени.
 *
 * @package App\Repositories\DTO
 */
class TrackRepositoryResultItem implements ITrackRepositoryResultItem
{
    /**
     * TrackRepositoryResultItem constructor.
     *
     * @param string $name
     * @param string $typeCode
     */
    public function __construct(private string $name, private string $typeCode)
    {
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getTypeCode(): string
    {
        return $this->typeCode;
    }
}
