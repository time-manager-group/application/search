<?php

namespace App\Repositories;

use App\Repositories\DTO\TrackRepositoryResultItem;
use App\Services\TracksFinder\Interfaces\ITrackRepository;
use App\Services\TracksFinder\Interfaces\ITrackRepositoryResultItem;
use App\Tools\TrackHelper;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\Pure;

/**
 * Class TrackRepository.
 * Репозиторий треков времени.
 *
 * @package App\Repositories
 */
class TrackRepository implements ITrackRepository
{
    /**
     * Лимит по запросы из базы данных.
     */
    private const ROWS_LIMIT = 5;

    /**
     * Метод поиска.
     *
     * @param string $text
     * @param array $excluded
     * @return ITrackRepositoryResultItem[]
     */
    public function find(string $text, array $excluded): array
    {
        $result = $this->makeBuilder()
            ->where('tracks.name', 'like', '%' . $text . '%')
            ->whereNotIn('tracks.name', $excluded)
            ->get();

        return $this->makeResult($result);
    }

    /**
     * Найти по поисковому слову.
     *
     * @param string $text
     * @return ITrackRepositoryResultItem[]
     */
    public function findByText(string $text): array
    {
        $tokens = TrackHelper::getTokens($text);
        $worlds = array_unique(explode(' ', $text));

        $builder = $this->makeBuilder()
            ->where('tracks.name', 'like', '%' . $text . '%');
        $builder->orderByRaw('if (`tracks`.`name` like ?, 1, 0) desc', '%' . $text . '%');
        $orderLikeSum = [];
        $orderLikeSumBind = [];

        foreach ($tokens as $token) {
            $builder->orWhere('tracks.name', 'like', '%' . $token . '%');
            $orderLikeSum[] = 'if (`tracks`.`name` like ?, 1, 0)';
            $orderLikeSumBind[] = '%' . $token . '%';
        }

        foreach ($worlds as $world) {
            $builder->orWhere('tracks.name', 'like', '%' . $world . '%');
            $orderLikeSum[] = 'if (`tracks`.`name` like ?, 1, 0)';
            $orderLikeSumBind[] = '%' . $world . '%';
        }

        if (count($orderLikeSum) > 0) {
            $builder->orderByRaw('(' . implode('+', $orderLikeSum) . ') desc', $orderLikeSumBind);
            $builder->orderByRaw('((' . implode('+', $orderLikeSum) . ')/(length(`tracks`.`name`) - length(replace(`tracks`.`name`, \' \', \'\')) + 1)) desc', $orderLikeSumBind);
        }

        $result = $builder->get();

        return $this->makeResult($result);
    }

    /**
     * Распечатка sql запроса со вставленными биндами.
     */
    private static function getSqlWithBindings($query): string
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }

    /**
     * Найти, исключая.
     *
     * @param array $excluded
     * @return ITrackRepositoryResultItem[]
     */
    public function findByExcluded(array $excluded): array
    {
        $result = $this->makeBuilder()
            ->whereNotIn('tracks.name', $excluded)
            ->get();

        return $this->makeResult($result);
    }

    /**
     * Найти все.
     *
     * @return ITrackRepositoryResultItem[]
     */
    public function findAll(): array
    {
        $result = $this->makeBuilder()
            ->get();

        return $this->makeResult($result);
    }

    /**
     * Создать билдера запроса.
     *
     * @return Builder
     */
    private function makeBuilder(): Builder
    {
        return DB::table('tracks')
            ->addSelect('tracks.name')
            ->addSelect('track_types.code')
            ->leftJoin('track_types', 'tracks.type_id', 'track_types.id')
            ->groupBy('tracks.name')
            ->groupBy('track_types.code')
            ->limit(self::ROWS_LIMIT);
    }

    /**
     * Создать результат.
     *
     * @param Collection $queryResult
     * @return ITrackRepositoryResultItem[]
     */
    #[Pure] private function makeResult(Collection $queryResult): array
    {
        $elements = [];

        foreach ($queryResult as $item) {
            $name = $item->name;
            $code = $item->code;
            $elements[] = new TrackRepositoryResultItem($name, $code);
        }

        return $elements;
    }
}
