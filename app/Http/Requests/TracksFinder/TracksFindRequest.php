<?php

namespace App\Http\Requests\TracksFinder;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class TracksFindRequest.
 * Валидация запроса по поиску треков.
 *
 * @package App\Http\Requests\TracksFinder
 */
class TracksFindRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    #[ArrayShape(['query' => "string", 'excluded' => "string", 'excluded.*' => "string"])]
    public function rules(): array
    {
        return [
            'query'      => 'string|nullable',
            'excluded'   => 'array',
            'excluded.*' => 'string',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return string[]
     */
    #[ArrayShape(['query.string' => "string", 'excluded.*' => "string"])]
    public function messages(): array
    {
        return [
            'query.string' => 'A query is type string',
            'excluded.*'   => 'A excluded is array string',
        ];
    }
}
