<?php

namespace App\Http\Controllers;

use App\Adaptors\RequestToDTO\TracksFinder\TracksFind;
use App\Services\TracksFinder\TracksFinder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TracksFindController.
 * Контроллер поиска треков времени.
 *
 * @package App\Http\Controllers
 */
class TracksFindController extends Controller
{
    /**
     * Запрос поиска.
     *
     * @param TracksFinder $tracksFinder
     * @param TracksFind   $tracksFind
     * @return Response
     */
    public function find(TracksFinder $tracksFinder, TracksFind $tracksFind): Response
    {
        return $this
            ->serializer
            ->serialize($tracksFinder->find($tracksFind));
    }
}
