<?php

namespace App\Http\Controllers;

use JetBrains\PhpStorm\ArrayShape;

/**
 * Class HealthCheckController.
 * Отвечает за доступность сервиса.
 *
 * @package App\Http\Controllers
 */
class HealthCheckController extends Controller
{
    /**
     * Пинг сервиса.
     *
     * @return array
     */
    #[ArrayShape(['i' => "string", 'ready' => "bool"])]
    public function ping(): array
    {
        return [
            'i'     => 'app_back_search',
            'ready' => true,
        ];
    }
}
