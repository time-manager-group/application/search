<?php

namespace App\Adaptors\RequestToDTO\TracksFinder;

use App\Http\Requests\TracksFinder\TracksFindRequest;
use App\Services\TracksFinder\Interfaces\ITracksFindData;
use Illuminate\Support\Arr;

/**
 * Class TracksFind.
 * Адаптер данных из request в сервис поиска треков времени.
 *
 * @package App\Adaptors\RequestToDTO\TracksFinder
 */
class TracksFind implements ITracksFindData
{
    /**
     * @var string|null
     */
    private ?string $query;
    /**
     * @var string[]|null
     */
    private ?array $excluded;

    /**
     * TracksFind constructor.
     *
     * @param TracksFindRequest $tracksFindRequest
     */
    public function __construct(TracksFindRequest $tracksFindRequest)
    {
        $all = $tracksFindRequest->all();

        $this->query    = Arr::get($all, 'query');
        $this->excluded = Arr::get($all, 'excluded');
    }

    /**
     * @inherit
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }


    /**
     * @inherit
     */
    public function getExcluded(): ?array
    {
        return $this->excluded;
    }
}
