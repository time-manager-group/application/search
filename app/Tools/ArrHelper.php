<?php

namespace App\Tools;

use Illuminate\Support\Arr;

class ArrHelper
{
    public static function hash(array $arr): string
    {
        return md5(json_encode($arr));
    }

    public static function eq(array $arr1, array $arr2): string
    {
        return self::hash($arr1) == self::hash($arr2);
    }

    public static function eqByFields(array $arr1, array $arr2, array|string $fields): bool
    {
        if (!is_array($fields)) {
            $fields = [$fields];
        }
        foreach ($fields as $field) {
            if (!array_key_exists($field, $arr1)) {
                return false;
            }
            if (!array_key_exists($field, $arr2)) {
                return false;
            }
            if ($arr1[$field] !== $arr2[$field]) {
                return false;
            }
        }
        return true;
    }

    public static function eqListByFields(array $arr1, array $arr2, array|string $fields): bool
    {
        if (count($arr1) !== count($arr2)) {
            return false;
        }
        foreach ($arr1 as $key => $value) {
            if (!array_key_exists($key, $arr2)) {
                return false;
            }
            if (!self::eqByFields($value, $arr2[$key], $fields)) {
                return false;
            }
        }
        return true;
    }

    public static function uniqListByFields(array $arr1, array|string|callable $fields = []): array
    {
        $arr = [];
        $callback = null;
        if (is_callable($fields)) {
            $callback = $fields;
        }
        if (!is_array($fields)) {
            $fields = [$fields];
        }
        foreach ($arr1 as $item) {
            $key = self::hash($callback ? $callback($item) : Arr::except($item, $fields ?: array_keys($item)));
            $arr[$key] = $item;
        }
        return array_values($arr);
    }
}
