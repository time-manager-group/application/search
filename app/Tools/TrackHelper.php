<?php

namespace App\Tools;

use Illuminate\Support\Str;

class TrackHelper
{
    public const TOKEN_SEPARATOR = ':';

    public static function getPreparedName(string $name): string
    {
        return Str::lower(trim(preg_replace('/[.|:]/ui', self::TOKEN_SEPARATOR, preg_replace('/\s+/ui', ' ', $name))));
    }

    /**
     * @return string[]
     */
    public static function getTokens(string $name): array
    {
        return array_map(function (string $token): string {
            return trim($token);
        }, explode(self::TOKEN_SEPARATOR, self::getPreparedName($name)));
    }

    public static function getTokenObjects(string $name): array
    {
        $delimiters = ['.', '|', ':'];
        $clearedName = trim(preg_replace('/\s+/ui', ' ', $name));

        // Создание регулярного выражения для делимитеров
        $pattern = '/' . implode('|', array_map('preg_quote', $delimiters)) . '/';

        // Массив для хранения результатов
        $result = [];
        $lastPos = 0;

        // Используем preg_match_all для поиска всех соответствий
        if (preg_match_all($pattern, $clearedName, $matches, PREG_OFFSET_CAPTURE)) {
            foreach ($matches[0] as $match) {
                $delimiterUsed = $match[0]; // найденный разделитель
                $position = $match[1]; // позиция разделителя в строке

                // Получаем подстроку между последним разделителем и текущим
                $substring = substr($clearedName, $lastPos, $position - $lastPos);

                // Добавляем подстроку и используемый разделитель в результат
                if ($substring !== '') {
                    $result[] = [
                        'value' => $substring,
                        'delimiter' => $delimiterUsed,
                        'token' => Str::lower(trim($substring)),
                    ];
                }

                // Обновляем последнюю позицию
                $lastPos = $position + strlen($delimiterUsed);
            }

            // Добавляем последний элемент после последнего разделителя
            if ($lastPos < strlen($clearedName)) {
                $result[] = [
                    'value' => substr($clearedName, $lastPos),
                    'delimiter' => null, // нет разделителя после последнего элемента
                    'token' => Str::lower(trim(substr($clearedName, $lastPos))),
                ];
            }
        }

        return $result;
    }

    public static function getNameByTokenObjects(array $tokenObjects): string
    {
        $nameArr = [];
        foreach ($tokenObjects as $tokenObject) {
            $nameArr[] = $tokenObject['value'];
            if (!is_null($tokenObject['delimiter'])) {
                $nameArr[] = $tokenObject['delimiter'] === '|' ? ' |' : $tokenObject['delimiter'];
            }
        }
        return implode('', $nameArr);
    }
}
