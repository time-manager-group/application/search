<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */

namespace App\Providers;

use App\Repositories\TrackRepository;
use App\Services\TracksFinder\Interfaces\ITrackRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider.
 * Провайдер приложения поиска.
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    public function register()
    {
        app()->bind(ITrackRepository::class, TrackRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
