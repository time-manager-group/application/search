<?php

namespace App\Services\TracksFinder;

use App\Repositories\DTO\TrackRepositoryResultItem;
use App\Services\TracksFinder\Interfaces\ITrackData;
use App\Services\TracksFinder\Interfaces\ITrackRepository;
use App\Services\TracksFinder\Interfaces\ITrackRepositoryResultItem;
use App\Services\TracksFinder\Interfaces\ITracksFindData;
use App\Tools\ArrHelper;
use App\Tools\TrackHelper;
use Illuminate\Support\Arr;

/**
 * Class TracksFinder.
 * Сервис поиска треков времени.
 *
 * @package App\Services\TracksFinder
 */
class TracksFinder
{
    /**
     * TracksFinder constructor.
     *
     * @param ITrackRepository $trackRepository
     */
    public function __construct(
        private ITrackRepository $trackRepository
    ) {

    }

    /**
     * Метод поиска.
     *
     * @param ITracksFindData $tracksFindData
     * @return ITrackData[]
     * @noinspection NotOptimalIfConditionsInspection
     */
    public function find(ITracksFindData $tracksFindData): array
    {
        if ($tracksFindData->getQuery() && $tracksFindData->getExcluded()) {
            $repositoryResult = $this->trackRepository->find(
                $this->clearString($tracksFindData->getQuery()),
                $this->clearArrayString($tracksFindData->getExcluded()));
        } elseif ($tracksFindData->getQuery()) {
            $repositoryResult = $this->trackRepository->findByText(
                $this->clearString($tracksFindData->getQuery())
            );

        } elseif ($tracksFindData->getExcluded()) {
            $repositoryResult = $this->trackRepository->findByExcluded(
                $this->clearArrayString($tracksFindData->getExcluded())
            );

        } else {
            $repositoryResult = $this->trackRepository->findAll();
        }

        $tokenObjects = TrackHelper::getTokenObjects($tracksFindData->getQuery());
        $tokens = TrackHelper::getTokens($tracksFindData->getQuery());

        foreach ($repositoryResult as $item) {
            $name = $item->getName();
            $code = $item->getTypeCode();
            $itemTokens = TrackHelper::getTokens($name);

            $mapIntersectTokens = [];
            foreach ($tokens as $token) {
                foreach ($itemTokens as $itemToken) {
                    if (mb_strpos($itemToken, $token) !== false) {
                        $mapIntersectTokens[$token] = $itemToken;
                    }
                }
            }

            $itemTokenObjects = TrackHelper::getTokenObjects($name);
            $newTokenObjects = [];
            foreach ($tokenObjects as $tokenObject) {
                $similarToken = $mapIntersectTokens[$tokenObject['token']] ?? null;
                if ($similarToken) {
                    $exists = array_filter($itemTokenObjects, function ($itemTokenObject) use ($similarToken) {
                        return $itemTokenObject['token'] === $similarToken;
                    });
                    if (is_null(Arr::first($exists))) {
                        $newTokenObjects[] = $tokenObject;
                    } else {
                        $newTokenObjects[] = Arr::first($exists);
                    }
                } else {
                    $newTokenObjects[] = $tokenObject;
                }

            }

            if (count($newTokenObjects) > 0 && !ArrHelper::eqListByFields($newTokenObjects, $tokenObjects, 'token')) {
                $newTokenObjects[count($newTokenObjects) - 1]['delimiter'] = null;
                foreach ($newTokenObjects as $key => $newTokenObject) {
                    $newTokenObjects[$key]['value'] = trim($newTokenObject['value']);
                    if ($key > 0) {
                        $newTokenObjects[$key]['value'] = " {$newTokenObjects[$key]['value']}";
                    }
                }
                $repositoryResult = Arr::prepend($repositoryResult, new TrackRepositoryResultItem(TrackHelper::getNameByTokenObjects($newTokenObjects), $code));
            }
        }

        $repositoryResult = ArrHelper::uniqListByFields($repositoryResult, function (TrackRepositoryResultItem $item): array {
            return [
                'name' => $item->getName(),
                'code' => $item->getTypeCode(),
            ];
        });

        return $this->makeResult($repositoryResult);
    }

    /**
     * Отчищает строку от лишних пробелов, приводит к слова к нижнему регистру.
     *
     * @param string $text
     * @return string
     */
    private function clearString(string $text): string
    {
        $text = mb_strtolower($text);
        $text = trim($text);

        return preg_replace('/\s+/u', ' ', $text);
    }

    /**
     * Отчищает массив строк методом clearString.
     *
     * @param array $arrayString
     * @return string[]
     */
    private function clearArrayString(array $arrayString): array
    {
        foreach ($arrayString as &$itemString) {
            $itemString = $this->clearString($itemString);
        }

        unset($itemString);

        return $arrayString;
    }

    /**
     * @param ITrackRepositoryResultItem[] $repositoryResult
     * @return  ITrackData[]
     */
    private function makeResult(array $repositoryResult): array
    {
        $result = [];

        foreach ($repositoryResult as $item) {
            $result [] = (new TrackData())
                ->setName($item->getName())
                ->setType($item->getTypeCode());
        }

        return $result;
    }
}
