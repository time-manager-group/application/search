<?php

namespace App\Services\TracksFinder\Interfaces;

/**
 * Interface ITrackRepositoryResultItem.
 * Интерфейс результата репозитория по поиску треков времени.
 *
 * @package App\Services\TracksFinder\Interfaces
 */
interface ITrackRepositoryResultItem
{
    /**
     * Наименование трека.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Код типа трека.
     *
     * @return string
     */
    public function getTypeCode(): string;
}
