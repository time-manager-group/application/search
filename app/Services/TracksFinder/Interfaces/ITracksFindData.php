<?php

namespace App\Services\TracksFinder\Interfaces;

/**
 * Interface ITracksFindData.
 * Интерфейс для данных поиска треков времени.
 *
 * @package App\Services\TracksFinder
 */
interface ITracksFindData
{
    /**
     * Текстовый запрос.
     *
     * @return string|null
     */
    public function getQuery(): ?string;

    /**
     * Список, который исключает наименование треков времени из поиска.
     *
     * @return string[]|null
     */
    public function getExcluded(): ?array;
}
