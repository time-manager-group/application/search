<?php

namespace App\Services\TracksFinder\Interfaces;

/**
 * Interface ITrackData.
 * Интерфейс найденного трека времени.
 *
 * @package App\Services\TracksFinder
 */
interface ITrackData
{
    /**
     * Наименование трека.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Код типа трека.
     *
     * @return string
     */
    public function getType(): string;
}
