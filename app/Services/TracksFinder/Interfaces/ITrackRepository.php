<?php

namespace App\Services\TracksFinder\Interfaces;

/**
 * Interface ITrackRepository.
 * Интерфейс репозитория треков времени.
 *
 * @package App\Services\TracksFinder
 */
interface ITrackRepository
{
    /**
     * Метод поиска по всем параметрам.
     *
     * @param string $text
     * @param array  $excluded
     * @return ITrackRepositoryResultItem[]
     */
    public function find(string $text, array $excluded): array;

    /**
     * Метод поиска по поисковом слову.
     *
     * @param string $text
     * @return ITrackRepositoryResultItem[]
     */
    public function findByText(string $text): array;

    /**
     * Метод поиска с исключениями.
     *
     * @param array $excluded
     * @return ITrackRepositoryResultItem[]
     */
    public function findByExcluded(array $excluded): array;

    /**
     * Метод поиска без параметров.
     *
     * @return ITrackRepositoryResultItem[]
     */
    public function findAll(): array;
}
