<?php

namespace App\Services\TracksFinder;

/**
 * Class TrackData.
 * Данные найденного трека времени.
 *
 * @package App\Services\TracksFinder
 */
class TrackData
{
    private string $name;
    private string $type;

    /**
     * Имя трека.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Тип трека.
     *
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Имя трека.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Тип трека.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
