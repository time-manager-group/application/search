<?php

use App\Http\Controllers\HealthCheckController;
use App\Http\Controllers\TracksFindController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/healthcheck', [HealthCheckController::class, 'ping']);
Route::post('/track', [TracksFindController::class, 'find']);

