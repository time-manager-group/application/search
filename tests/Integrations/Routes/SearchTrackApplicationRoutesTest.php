<?php

namespace Tests\Integrations\Routes;

use Illuminate\Testing\TestResponse;
use Symfony\Component\VarDumper\VarDumper;
use Tests\QueryResults\SearchQueryResult;
use Tests\TestCase;
use Throwable;

/**
 * Class SearchTrackApplicationRoutesTest.
 * Интеграционное тестирование поиска по трекам с уровня http запроса.
 *
 * @package Tests\Routes
 */
class SearchTrackApplicationRoutesTest extends TestCase
{
    /**
     * Проверяем поиск по трекам.
     */
    public function testFindTracks(): void
    {
        $reportList = [];

        foreach (SearchQueryResult::getTestingList() as $item) {

            $response = $this->postJson('track', [
                'query'    => $item['text'],
                'excluded' => $item['excluded'],
            ]);

            try {
                $this->assertFindTracks($item, $response);
                $this->addReport($reportList, $item, $response, true);
            } catch (Throwable $throwable) {
                $this->addReport($reportList, $item, $response, false, $throwable);
            }
        }

        $this->checkReport($reportList);

    }

    private function checkReport(array $reportList): void
    {
        $failCount = 0;
        foreach ($reportList as $key => $item) {
            if (!$item['success']) {
                VarDumper::dump('Неуспешный кейс № ' . ($failCount + 1) . ' , из всех по номеру ' . ($key + 1));
                VarDumper::dump($item);
                $failCount++;
            }
        }

        if ($failCount) {
            self::fail("Количество неуспешно протестированных кейсов $failCount из " . count($reportList));
        }
    }

    /**
     * Тестирование поиска треков времени.
     *
     * @param array        $item
     * @param TestResponse $response
     */
    private function assertFindTracks(array $item, TestResponse $response): void
    {
        $countElements = count($item['result']);

        $this->assertResponse($response, $countElements);
        $response->assertJson($item['result']);

        if (!$item['text']) {
            $response = $this->postJson('track', [
                'excluded' => $item['excluded'],
            ]);

            $this->assertResponse($response, $countElements);
            $response->assertJson($item['result']);
        } elseif (!$item['excluded']) {
            $response = $this->postJson('track', [
                'query' => $item['text'],
            ]);

            $this->assertResponse($response, $countElements);
            $response->assertJson($item['result']);
        } elseif (!$item['text'] && !$item['excluded']) {
            $response = $this->postJson('track', [
            ]);

            $this->assertResponse($response, $countElements);
            $response->assertJson($item['result']);
        }
    }

    private function addReport(
        array &$reportList,
        array $item,
        TestResponse $response,
        bool $success,
        Throwable $throwable = null
    ): void {
        $reportList[] = [
            'success' => $success,
            'waiting' => $item,
            'reality' => $response->json(),
            'message' => $throwable?->getMessage(),
        ];
    }




    /**
     * Проверка успешного ответа на запрос.
     *
     * @param TestResponse $response
     * @param int          $countElements
     */
    private function assertResponse(TestResponse $response, int $countElements): void
    {
        $response->assertOk();
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonStructure(
            $countElements ? [
                [
                    'name',
                    'type',
                ],
            ] : []
        );

        if ($countElements) {
            $response->assertJsonCount(2, '0.*');
        }

        $this->assertMaxCount($response->json());
    }

    /**
     * Проверка поверка количества вернувшихся элементов.
     *
     * @param $list
     */
    private function assertMaxCount($list): void
    {
        if (count($list) > 5) {
            self::fail('Количество элементов больше 5');
        }
    }
}
