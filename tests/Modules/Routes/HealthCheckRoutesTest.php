<?php

namespace Tests\Modules\Routes;

use Tests\TestCase;

/**
 * Class HealthCheckRoutesTest.
 * Модульное тестирование роута о доступности сервиса.
 *
 * @package Tests\Routes
 */
class HealthCheckRoutesTest extends TestCase
{
    /**
     * Тест роута о доступности сервиса.
     */
    public function testHealthCheckRoute(): void
    {
        $response = $this->get('healthcheck');

        $response->assertStatus(200);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJson([
            'i'     => 'app_back_search',
            'ready' => true,
        ]);
        $response->assertJsonCount(2);
    }
}
