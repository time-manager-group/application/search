<?php

namespace Tests\Modules\Routes;

use App\Services\TracksFinder\TracksFinder;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

/**
 * Class SearchTrackValidationRoutesTest.
 * Модульное тестирование валидации на роутах сервиса по поиску треков времени.
 *
 * @package Tests\Modules\Routes
 */
class SearchTrackValidationRoutesTest extends TestCase
{
    /**
     * Переопределяем сервис на заглушку.
     */
    private function bindFakeService(): void
    {
        $fakeService = $this->getMockBuilder(TracksFinder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $fakeService->method('find')
            ->willReturn([]);

        app()->bind(TracksFinder::class, static function () use ($fakeService) {
            return $fakeService;
        });
    }

    /**
     * Тест неверного синтаксиса параметра `query`.
     */
    public function testInvalidQueryParam(): void
    {
        $this->bindFakeService();

        $response = $this->postJson('track', [
            'query' => ['test word'],
        ]);

        $this->assertInvalidParamsResponse($response, ['query']);

        $response = $this->postJson('track', [
            'query' => 123,
        ]);

        $this->assertInvalidParamsResponse($response, ['query']);
    }

    /**
     * Тест неверного синтаксиса параметра `excluded`.
     */
    public function testInvalidExcludedParam(): void
    {
        $this->bindFakeService();

        $response = $this->postJson('track', [
            'excluded' => 'test word',
        ]);

        $this->assertInvalidParamsResponse($response, ['excluded']);

        $response = $this->postJson('track', [
            'excluded' => 123,
        ]);

        $this->assertInvalidParamsResponse($response, ['excluded']);

        $response = $this->postJson('track', [
            'excluded' => [123, 456],
        ]);

        $this->assertInvalidParamsResponse($response,
            [
                'excluded.0',
                'excluded.1',
            ]);

        $response = $this->postJson('track', [
            'excluded' => [['test'], ['tes2']],
        ]);

        $this->assertInvalidParamsResponse($response,
            [
                'excluded.0',
                'excluded.1',
            ]);
    }

    /**
     * Тест неверного синтаксиса параметра  `query` и `excluded`.
     */
    public function testInvalidQueryAndExcludedParam(): void
    {
        $this->bindFakeService();

        $response = $this->postJson('track', [
            'query'    => ['test word'],
            'excluded' => 'test word',
        ]);

        $this->assertInvalidParamsResponse($response, ['query', 'excluded']);
    }

    /**
     * Проверка ответа невалидных данных.
     *
     * @param TestResponse $response
     * @param string[]     $fields
     */
    private function assertInvalidParamsResponse(TestResponse $response, array $fields = []): void
    {
        $response->assertStatus(422);
        $response->assertHeader('content-type', 'application/json');
        $response->assertJsonCount(2);
        $response->assertJsonCount(count($fields), 'errors');
        $responseMessage = $response->json()['message'];

        if (!$responseMessage) {
            self::fail('Параметр `message` должен быть не пустым');
        }

        if (!is_string($responseMessage)) {
            self::fail('Параметр `message` должен быть строковым типом');
        }

        $response->assertJsonValidationErrors($fields);
    }
}
