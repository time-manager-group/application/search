<?php

namespace Tests\Modules\Services\TracksFinder;

use App\Services\TracksFinder\Interfaces\ITrackRepositoryResultItem;
use App\Services\TracksFinder\Interfaces\ITrackRepository;
use App\Services\TracksFinder\Interfaces\ITracksFindData;
use App\Services\TracksFinder\TrackData;
use App\Services\TracksFinder\TracksFinder;
use JetBrains\PhpStorm\Pure;
use Tests\TestCase;

/**
 * Class TracksFinderTest.
 * Модульное тестирование сервиса TracksFinder.
 *
 * @package Tests\Services\TracksFinder
 */
class TracksFinderTest extends TestCase
{
    /**
     * Тест проверки при наличии всех параметров.
     */
    public function testFindWithNotEmptyRepositoryAndAllParams(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('test', ['test', 'test']));
        $realityResult = [
            $this->mockResultFindDto('test 1', 'code1'),
            $this->mockResultFindDto('test 2', 'code2'),
            $this->mockResultFindDto('test 3', 'code3'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при пустом значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndQueryEmptyParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('', ['test', 'test']));
        $realityResult = [
            $this->mockResultFindDto('test 7', 'code7'),
            $this->mockResultFindDto('test 8', 'code8'),
            $this->mockResultFindDto('test 9', 'code9'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при null значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndQueryNullParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto(null, ['test', 'test']));
        $realityResult = [
            $this->mockResultFindDto('test 7', 'code7'),
            $this->mockResultFindDto('test 8', 'code8'),
            $this->mockResultFindDto('test 9', 'code9'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при пустом значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndExcludedEmptyParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('test', []));
        $realityResult = [
            $this->mockResultFindDto('test 4', 'code4'),
            $this->mockResultFindDto('test 5', 'code5'),
            $this->mockResultFindDto('test 6', 'code6'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при null значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndExcludedNullParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('test', null));
        $realityResult = [
            $this->mockResultFindDto('test 4', 'code4'),
            $this->mockResultFindDto('test 5', 'code5'),
            $this->mockResultFindDto('test 6', 'code6'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при null query и пустом значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndAndQueryNullAndExcludedEmptyParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto(null, []));
        $realityResult = [
            $this->mockResultFindDto('test 10', 'code10'),
            $this->mockResultFindDto('test 11', 'code11'),
            $this->mockResultFindDto('test 12', 'code12'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при пустом query и пустом значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndQueryEmptyAndExcludedEmptyParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('', []));
        $realityResult = [
            $this->mockResultFindDto('test 10', 'code10'),
            $this->mockResultFindDto('test 11', 'code11'),
            $this->mockResultFindDto('test 12', 'code12'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при пустом query и null значении excluded.
     */
    public function testFindWithNotEmptyRepositoryAndQueryEmptyAndExcludedNullParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('', null));
        $realityResult = [
            $this->mockResultFindDto('test 10', 'code10'),
            $this->mockResultFindDto('test 11', 'code11'),
            $this->mockResultFindDto('test 12', 'code12'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при null значении всех параметров.
     */
    public function testFindWithNotEmptyRepositoryAndNullAllParam(): void
    {
        $repository = $this->mockNotEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto(null, null));
        $realityResult = [
            $this->mockResultFindDto('test 10', 'code10'),
            $this->mockResultFindDto('test 11', 'code11'),
            $this->mockResultFindDto('test 12', 'code12'),
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при наличии всех параметров при пустом репозитории.
     */
    public function testFindWithEmptyRepositoryAndAllParams(): void
    {
        $repository = $this->mockEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('test', ['test', 'test']));
        $realityResult = [
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест проверки при null значении всех параметров при пустом репозитории.
     */
    public function testFindWithEmptyRepositoryAndNullAllParam(): void
    {
        $repository = $this->mockEmptyRepository();

        $service = $this->mockService($repository);

        $result        = $service->find($this->mockParamFindDto('test', ['test', 'test']));
        $realityResult = [
        ];

        self::assertEquals($realityResult, $result);
    }

    /**
     * Тест передаваемых аргументов в метод репозитория find.
     */
    public function testFindWithRepositoryMethodFindArgumentsParam(): void
    {
        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('find')
            ->with(self::equalTo('test'), self::equalTo(['test', 'test']));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto('test', ['test', 'test']));
        $service->find($this->mockParamFindDto('test ', ['test', 'test']));
        $service->find($this->mockParamFindDto(' test ', ['test', 'test']));
        $service->find($this->mockParamFindDto(' test ', [' test', 'test']));
        $service->find($this->mockParamFindDto(' test ', [' test', 'test ']));

        $service->find($this->mockParamFindDto(' Test ', [' test', 'test ']));
        $service->find($this->mockParamFindDto(' TESt ', [' test', 'test ']));
        $service->find($this->mockParamFindDto(' test ', [' test', 'TEst ']));

        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('find')
            ->with(self::equalTo('test test'), self::equalTo(['test', 'test']));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto(' test   test ', [' test', 'test ']));
        $service->find($this->mockParamFindDto(' tEST   test ', [' TEst', 'test ']));

        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('find')
            ->with(self::equalTo('test test'), self::equalTo(['test test', 'test']));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto(' test   test ', [' test    test    ', 'test ']));
        $service->find($this->mockParamFindDto(' TEst   teST ', [' TEst    tEst    ', 'tEst ']));

        self::assertTrue(true);
    }

    /**
     * Тест передаваемых аргументов в метод репозитория findByText.
     */
    public function testFindWithRepositoryMethodFindByTextArgumentParam(): void
    {
        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('findByText')
            ->with(self::equalTo('test'));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto('test', []));
        $service->find($this->mockParamFindDto('test ', []));
        $service->find($this->mockParamFindDto(' Test ', []));
        $service->find($this->mockParamFindDto(' TESt ', []));

        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('findByText')
            ->with(self::equalTo('test test'));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto(' test   test ', []));
        $service->find($this->mockParamFindDto(' tEST   test ', []));

        self::assertTrue(true);
    }

    /**
     * Тест передаваемых аргументов в метод репозитория findByExcluded.
     */
    public function testFindWithRepositoryMethodFindByExcludedArgumentParam(): void
    {
        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('findByExcluded')
            ->with(self::equalTo(['test', 'test']));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto('', ['test', 'test']));
        $service->find($this->mockParamFindDto(null, [' test', 'test']));
        $service->find($this->mockParamFindDto('', [' test', 'test ']));
        $service->find($this->mockParamFindDto(null, [' test', 'TEst ']));
        $service->find($this->mockParamFindDto('', [' test', 'test ']));
        $service->find($this->mockParamFindDto(null, [' TEst', 'test ']));

        $stubRepository = $this->createMock(ITrackRepository::class);
        $stubRepository->method('findByExcluded')
            ->with(self::equalTo(['test test', 'test']));
        $service = $this->mockService($stubRepository);

        $service->find($this->mockParamFindDto('', [' test    test    ', 'test ']));
        $service->find($this->mockParamFindDto(null, [' TEst    tEst    ', 'tEst ']));

        self::assertTrue(true);
    }

    /**
     * Создать сервис.
     *
     * @param ITrackRepository $trackRepository
     * @return TracksFinder
     */
    #[Pure] private function mockService(ITrackRepository $trackRepository): TracksFinder
    {
        return new TracksFinder($trackRepository);
    }

    /**
     * Создать входящую DTO.
     *
     * @param $query
     * @param $excluded
     * @return ITracksFindData
     */
    private function mockParamFindDto($query, $excluded): ITracksFindData
    {
        return new class($query, $excluded) implements ITracksFindData {
            public function __construct(private $query, private $excluded)
            {
            }

            public function getQuery(): ?string
            {
                return $this->query;
            }

            public function getExcluded(): ?array
            {
                return $this->excluded;
            }
        };
    }

    /**
     * Создать возвращающую DTO.
     *
     * @param $name
     * @param $type
     * @return TrackData
     */
    private function mockResultFindDto($name, $type): TrackData
    {
        return (new TrackData())->setName($name)->setType($type);
    }

    /**
     * Создать пустой репозиторий.
     *
     * @return ITrackRepository
     */
    private function mockEmptyRepository(): ITrackRepository
    {
        $stubRepository = $this->createMock(ITrackRepository::class);

        $dataFind       = [];
        $findByText     = [];
        $findByExcluded = [];
        $findAll        = [];

        $stubRepository->method('find')
            ->willReturn($dataFind);
        $stubRepository->method('findByText')
            ->willReturn($findByText);
        $stubRepository->method('findByExcluded')
            ->willReturn($findByExcluded);
        $stubRepository->method('findAll')
            ->willReturn($findAll);

        self::assertSame($dataFind, $stubRepository->find('test', ['test']));
        self::assertSame($findByText, $stubRepository->findByText('test'));
        self::assertSame($findByExcluded, $stubRepository->findByExcluded(['test']));
        self::assertSame($findAll, $stubRepository->findAll());

        return $stubRepository;
    }

    /**
     * Создать наполненный репозиторий.
     *
     * @return ITrackRepository
     */
    private function mockNotEmptyRepository(): ITrackRepository
    {
        $stubRepository = $this->createMock(ITrackRepository::class);

        $dataFind       = [
            $this->mockTrackRepositoryResultItemDto('test 1', 'code1'),
            $this->mockTrackRepositoryResultItemDto('test 2', 'code2'),
            $this->mockTrackRepositoryResultItemDto('test 3', 'code3'),
        ];
        $findByText     = [
            $this->mockTrackRepositoryResultItemDto('test 4', 'code4'),
            $this->mockTrackRepositoryResultItemDto('test 5', 'code5'),
            $this->mockTrackRepositoryResultItemDto('test 6', 'code6'),
        ];
        $findByExcluded = [
            $this->mockTrackRepositoryResultItemDto('test 7', 'code7'),
            $this->mockTrackRepositoryResultItemDto('test 8', 'code8'),
            $this->mockTrackRepositoryResultItemDto('test 9', 'code9'),
        ];
        $findAll        = [
            $this->mockTrackRepositoryResultItemDto('test 10', 'code10'),
            $this->mockTrackRepositoryResultItemDto('test 11', 'code11'),
            $this->mockTrackRepositoryResultItemDto('test 12', 'code12'),
        ];

        $stubRepository->method('find')
            ->willReturn($dataFind);
        $stubRepository->method('findByText')
            ->willReturn($findByText);
        $stubRepository->method('findByExcluded')
            ->willReturn($findByExcluded);
        $stubRepository->method('findAll')
            ->willReturn($findAll);

        self::assertSame($dataFind, $stubRepository->find('test', ['test']));
        self::assertSame($findByText, $stubRepository->findByText('test'));
        self::assertSame($findByExcluded, $stubRepository->findByExcluded(['test']));
        self::assertSame($findAll, $stubRepository->findAll());

        return $stubRepository;
    }


    /**
     * Создать возвращающую DTO из методов репозитория.
     *
     * @param $name
     * @param $type
     * @return ITrackRepositoryResultItem
     */
    private function mockTrackRepositoryResultItemDto($name, $type): ITrackRepositoryResultItem
    {
        $stubRepository = $this->createMock(ITrackRepositoryResultItem::class);

        $stubRepository->method('getName')
            ->willReturn($name);

        $stubRepository->method('getTypeCode')
            ->willReturn($type);

        return $stubRepository;
    }
}
