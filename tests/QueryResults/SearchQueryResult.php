<?php

namespace Tests\QueryResults;

/**
 * Class SearchQueryResult.
 * Варианты успешных результатов.
 *
 * @package Tests\QueryResults
 */
class SearchQueryResult
{
    private const SEEDS    = [
        100  => [
            'name' => 'Отдых',
            'type' => 'useless',
        ],
        200  => [
            'name' => 'Не работает доставка Laguna',
            'type' => 'undefined',
        ],
        300  => [
            'name' => 'Переговоры с клиентом Legacy',
            'type' => 'useful',
        ],
        400  => [
            'name' => 'Сломался докер',
            'type' => 'useless',
        ],
        500  => [
            'name' => 'Рефакторинг модуля поиска на проекте Учет времени',
            'type' => 'useless',
        ],
        600  => [
            'name' => 'Форма запроса цены на проекте Legacy',
            'type' => 'useful',
        ],
        700  => [
            'name' => 'Оценка по проектирования новой архитектуры ApiBot',
            'type' => 'useful',
        ],
        800  => [
            'name' => 'Консультация по проекту Legacy',
            'type' => 'useful',
        ],
        900  => [
            'name' => 'Афанасий чат',
            'type' => 'useless',
        ],
        1000 => [
            'name' => 'Тушение пожара на проекте Laguna',
            'type' => 'undefined',
        ],
        1100 => [
            'name' => 'Митап Docker',
            'type' => 'useless',
        ],
        1200 => [
            'name' => 'Принятие домашки у Христофора',
            'type' => 'undefined',
        ],
        1300 => [
            'name' => 'Афанасий чат',
            'type' => 'undefined',
        ],
        1400 => [
            'name' => 'Не работает доставка Laguna',
            'type' => 'undefined',
        ],
    ];
    private const VARIANTS = [
        [
            'text'     => 'отдых',
            'excluded' => [],
            'result'   => [
                self::SEEDS[100],
            ],
        ],
        [
            'text'     => 'Отдых',
            'excluded' => [],
            'result'   => [
                self::SEEDS[100],
            ],
        ],
        [
            'text'     => 'Отдых',
            'excluded' => ['Отдых'],
            'result'   => [
            ],
        ],
        [
            'text'     => 'отдых',
            'excluded' => ['отдых'],
            'result'   => [
            ],
        ],
        [
            'text'     => 'Проект',
            'excluded' => [],
            'result'   => [
                self::SEEDS[500],
                self::SEEDS[600],
                self::SEEDS[700],
                self::SEEDS[800],
                self::SEEDS[1000],
            ],
        ],
        [
            'text'     => 'Проект',
            'excluded' => [
                'Отдых',
                'Рефакторинг модуля поиска на проекте Учет времени',
                'Тушение пожара Laguna',
            ],
            'result'   => [
                self::SEEDS[600],
                self::SEEDS[700],
                self::SEEDS[800],
                self::SEEDS[1000],
            ],
        ],
        [
            'text'     => 'Чат',
            'excluded' => [],
            'result'   => [
                self::SEEDS[900],
                self::SEEDS[1300],
            ],
        ],
        [
            'text'     => 'lagu',
            'excluded' => [],
            'result'   => [
                self::SEEDS[200],
                self::SEEDS[1000],
            ],
        ],
        [
            'text'     => 'lagu',
            'excluded' => [
                'Не работает доставка Laguna',
            ],
            'result'   => [
                self::SEEDS[1000],
            ],
        ],
        [
            'text'     => 'lagu',
            'excluded' => [
                'Тушение пожара на проекте Laguna',
            ],
            'result'   => [
                self::SEEDS[200],
            ],
        ],
        [
            'text'     => 'lagu',
            'excluded' => [
                'Не работает доставка на проекте Laguna',
            ],
            'result'   => [
                self::SEEDS[200],
                self::SEEDS[1000],
            ],
        ],
        [
            'text'     => 'lagu',
            'excluded' => [
                'Не работает доставка Laguna',
                'Тушение пожара на проекте Laguna',
            ],
            'result'   => [
            ],
        ],
        [
            'text'     => 'о',
            'excluded' => [],
            'result'   => [
                self::SEEDS[100],
                self::SEEDS[200],
                self::SEEDS[300],
                self::SEEDS[400],
                self::SEEDS[500],
            ],
        ],
        [
            'text'     => 'о',
            'excluded' => [
                'Отдых',
            ],
            'result'   => [
                self::SEEDS[200],
                self::SEEDS[300],
                self::SEEDS[400],
                self::SEEDS[500],
                self::SEEDS[600],
            ],
        ],
        [
            'text'     => 'о',
            'excluded' => [
                'Отдых',
                'Переговоры с клиентом Legacy',
            ],
            'result'   => [
                self::SEEDS[200],
                self::SEEDS[400],
                self::SEEDS[500],
                self::SEEDS[600],
                self::SEEDS[700],
            ]
            ,
        ],
        [
            'text'     => '',
            'excluded' => [
            ],
            'result'   => [
                self::SEEDS[100],
                self::SEEDS[200],
                self::SEEDS[300],
                self::SEEDS[400],
                self::SEEDS[500],
            ],
        ],
        [
            'text'     => '',
            'excluded' => [
                'Отдых',
            ],
            'result'   => [
                self::SEEDS[200],
                self::SEEDS[300],
                self::SEEDS[400],
                self::SEEDS[500],
                self::SEEDS[600],
            ],
        ],
        [
            'text'     => '',
            'excluded' => [
                'Отдых',
                'Не работает доставка Laguna',
            ],
            'result'   => [
                self::SEEDS[300],
                self::SEEDS[400],
                self::SEEDS[500],
                self::SEEDS[600],
                self::SEEDS[700],
            ],
        ],
        [
            'text'     => '',
            'excluded' => [
                self::SEEDS[100]['name'],
                self::SEEDS[200]['name'],
                self::SEEDS[300]['name'],
                self::SEEDS[400]['name'],
                self::SEEDS[500]['name'],
                self::SEEDS[600]['name'],
                self::SEEDS[700]['name'],
                self::SEEDS[800]['name'],
                self::SEEDS[900]['name'],
            ],
            'result'   => [
                self::SEEDS[1000],
                self::SEEDS[1100],
                self::SEEDS[1200],
            ],
        ],
        [
            'text'     => 'Выпить кофе',
            'excluded' => [],
            'result'   => [],
        ],
        [
            'text'     => 'Выпить кофе',
            'excluded' => [
                self::SEEDS[100]['name'],
                self::SEEDS[200]['name'],
            ],
            'result'   => [],
        ],
    ];

    /**
     * Варианты запросов.
     *
     * @return array[]
     */
    public static function getTestingList(): array
    {
        return self::VARIANTS;
    }
}
